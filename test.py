from crawler import crawlerFunction

def test_length(n):
    l = crawlerFunction("https://ensai.fr/", n)
    assert len(l) <= n

if __name__ == "__main__":
    test_length(30)
    print("tout est ok")