from bs4 import BeautifulSoup
import urllib.request
import time
import urllib.robotparser

rp = urllib.robotparser.RobotFileParser()

def crawlerFunction(lien, limite):
    html_page = urllib.request.urlopen(lien)
    soup = BeautifulSoup(html_page, "html.parser")

    liste = []

    for link in soup.findAll('a'):
            liste.append(link.get('href'))

    if len(liste) >= limite:
        return liste[0:limite]
    else:
        for x in liste:
            time.sleep(5)
            if rp.can_fetch("*", x):
                liste += crawlerFunction(x, limite-len(liste))
            if len(liste) >= limite:
                return liste[0:limite]
    return liste
