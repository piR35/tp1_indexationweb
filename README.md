# CRAWLER EN PYTHON

Contributeur : Pierre Vasseur

## Explication du code

crawler.py contient la fonction permettant de crawler une url donnée dans une limite donnée.

test.py permet de tester cette fonction, et renvoie "tout est ok" si les tests sont concluants.

main.py permet de lancer le crawler avec l'url du site de l'ensai et une limite de 50.


## Execution

il suffit de lancer le fichier main.py pour récupérer le fichier texte des liens.

il suffit de lancer le fichier test.py pour tester la fonction crawler avec les assertions choisies.


