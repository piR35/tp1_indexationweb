from crawler import crawlerFunction

url = "https://ensai.fr/"
max = 50

if __name__ == "__main__":
    l = crawlerFunction(url, max)

    with open('crawled_webpages.txt', 'w') as f:
        for link in l:
            f.write(link)
            f.write('\n')